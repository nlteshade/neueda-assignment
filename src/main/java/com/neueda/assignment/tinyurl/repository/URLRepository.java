package com.neueda.assignment.tinyurl.repository;

import com.neueda.assignment.tinyurl.model.URLAddress;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface URLRepository extends CrudRepository<URLAddress, Integer> {

    @Query(value = "SELECT * FROM urls", nativeQuery = true)
    List<URLAddress> getAllURLs();
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM urls WHERE longURL = ?1 && shortURL = ?2", nativeQuery = true)
    void deleteByLongUrl(String longUrl, String shortUrl);
    @Modifying
    @Transactional
    @Query(value = "UPDATE urls SET shortURL = ?1 WHERE longUrl = ?2", nativeQuery = true)
    void update(String shortUrl, String longUrl);
    @Query(value = "SELECT longURL FROM urls WHERE shortURL = ?1 LIMIT 1", nativeQuery = true)
    String findByShortURL(String shortUrl);

}
