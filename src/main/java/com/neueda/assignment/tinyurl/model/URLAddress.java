package com.neueda.assignment.tinyurl.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "urls")
@EntityListeners(AuditingEntityListener.class)
public class URLAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String longURL;
    private String shortURL;

    public URLAddress() {
    }

    public URLAddress(String longURL, String shortURL) {
        this.longURL = longURL;
        this.shortURL = shortURL;
    }

    public URLAddress(int id, String longURL, String shortURL) {
        this.id = id;
        this.longURL = longURL;
        this.shortURL = shortURL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLongURL() {
        return longURL;
    }

    public void setLongURL(String longURL) {
        this.longURL = longURL;
    }

    public String getShortURL() {
        return shortURL;
    }

    public void setShortURL(String shortURL) {
        this.shortURL = shortURL;
    }
}
