package com.neueda.assignment.tinyurl.controller;

import com.neueda.assignment.tinyurl.model.URLAddress;
import com.neueda.assignment.tinyurl.repository.URLRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class URLController  {

    @Autowired
    private URLRepository urlRepository;

    @RequestMapping(value = "/createaurl", method = RequestMethod.POST)
    public void createATinyURL(@ModelAttribute("urlAddress") URLAddress urlAddress, Model model, HttpServletResponse response) {
        model.addAttribute("urlAddress", urlAddress);
        if(!urlAddress.getLongURL().isBlank()) {
            if(!urlAddress.getLongURL().toLowerCase().startsWith(("http://"))){
                urlAddress.setLongURL(new StringBuilder().append("http://").append(urlAddress.getLongURL()).toString());
            }
            urlRepository.save(urlAddress);
            try {
                response.sendRedirect("/");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                response.sendRedirect("/create");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @RequestMapping(value = "/updateaurl", method = RequestMethod.POST)
    public void updateATinyURL(@ModelAttribute("urlAddress") URLAddress urlAddress, Model model, HttpServletResponse response,
                               @RequestParam(value = "longURL", required = false) String longURL) {
        urlRepository.update(urlAddress.getShortURL(), longURL);
        try {
            response.sendRedirect("/index");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public void deletePage(Model model, HttpServletResponse response,
                             @RequestParam(value = "longURL", required = false) String longUrl,
                             @RequestParam(value = "shortURL", required = false) String shortUrl) {
        urlRepository.deleteByLongUrl(longUrl, shortUrl);
        model.addAttribute("urllist", urlRepository.getAllURLs());
        try {
            response.sendRedirect("/index");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/{shortUrl}", method = RequestMethod.GET)
    public void redirect(@PathVariable String shortUrl, HttpServletResponse httpServletResponse) {
        String longUrl = urlRepository.findByShortURL(shortUrl);
        httpServletResponse.setHeader("Location", longUrl);
        httpServletResponse.setStatus(302);
    }

}