package com.neueda.assignment.tinyurl.controller;

import com.neueda.assignment.tinyurl.model.URLAddress;
import com.neueda.assignment.tinyurl.repository.URLRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WebController {

    @Autowired
    private URLRepository urlRepository;

    @RequestMapping({"", "/", "/index"})
    public String getIndexPage(Model model) {
        model.addAttribute("urllist", urlRepository.getAllURLs());
        return "index";
    }

    @RequestMapping("/create")
    public String createPage(Model model) {
        model.addAttribute("urlAddress", new URLAddress("http://www.facebopok.com", "fb.com"));
        return "create";
    }

    @RequestMapping("/update")
    public String updatePage(Model model,  @RequestParam(value = "longURL", required = false) String longUrl) {
        model.addAttribute("urlAddress", new URLAddress("http://www.facebopok.com", "fb.com"));
        model.addAttribute("longURL", longUrl);
        return "update";
    }

}