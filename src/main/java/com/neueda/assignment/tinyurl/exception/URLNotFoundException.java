package com.neueda.assignment.tinyurl.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class URLNotFoundException extends RuntimeException {

    private String longUrl;

    public URLNotFoundException(String longUrl) {
        super(String.format(" not found : '%s'",longUrl));
        this.longUrl=longUrl;
    }

    public String getUrl() {
        return this.longUrl;
    }

}
