package com.neueda.assignment.tinyurl;

import com.neueda.assignment.tinyurl.controller.URLController;
import com.neueda.assignment.tinyurl.model.URLAddress;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class Tests {

    @Autowired
    private WebApplicationContext webApplicationContext;
    @InjectMocks
    private URLController urlController;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:11.1")
            .withDatabaseName("tinyurl")
            .withUsername("root")
            .withPassword("password");

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @Test
    public void createUrlEndpoint() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(urlController).build();
        mockMvc.perform(MockMvcRequestBuilders
                .post("/createaurl")
                .flashAttr("urlAddress",  new URLAddress("http://www.facebopok.com","fb.com"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateUrlEndpoint() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(urlController).build();
        mockMvc.perform(MockMvcRequestBuilders
                .post("/createaurl")
                .flashAttr("urlAddress",  new URLAddress("http://www.facebopok.com","fb.com"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        //assert that the entry exists in the database - fb.com for short url

        mockMvc.perform(MockMvcRequestBuilders
                .post("/updateaurl")
                .flashAttr("urlAddress",  new URLAddress("http://www.facebopok.com","test.com"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        //assert that the entry exists in the database - test.com for short url

    }

    @Test
    public void deleteUrlEndpoint() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(urlController).build();
        mockMvc.perform(MockMvcRequestBuilders
                .post("/createaurl")
                .flashAttr("urlAddress",  new URLAddress("http://www.facebopok.com","fb.com"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders
                .post("/delete")
                .param("longURL","http://www.facebopok.com")
                .param("shortURL","fb.com")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void redirect() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(urlController).build();
        mockMvc.perform(MockMvcRequestBuilders
                .post("/createaurl")
                .flashAttr("urlAddress",  new URLAddress("http://www.facebopok.com","fb.com"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders
                .post("/fb.com")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
