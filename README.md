<!-- PROJECT LOGO -->    
<br />    
<p align="center">    

<h3 align="center"></h3>

  <h1 align="center">Tiny URL Assignment</h1>    
    <hr/>  

<!-- TABLE OF CONTENTS -->    
<details open="open">    
  <summary>Table of Contents</summary>    
  <ol>    
    <li>    
      <a href="#about-the-project">About The Assignment</a>    
      <ul>    
        <li><a href="#built-with">Built With</a></li>    
		   <li><a href="#application-endpoints">Application Endpoints</a></li>
		   <li><a href="#statistic-endpoints">Statistic Endpoints</a></li>      
      </ul>    
    </li>    
    <li>    
      <a href="#getting-started">Getting Started</a>    
      <ul>    
        <li><a href="#prerequisites">Prerequisites</a></li>    
        <li><a href="#installation">Installation</a></li>    
      </ul>    
    </li>    
    <li><a href="#contact">Contact</a></li>    
  </ol>    
</details>    



<!-- ABOUT THE PROJECT -->    
## About The Assignment

### Description
Most of us are familiar with seeing URLs like bit.ly or t.co on our Twitter or Facebook    
feeds. These are examples of shortened URLs, which are a short alias or pointer to a    
longer page link. For example, I can send you the shortened URL http://bit.ly/SaaYw5 that will forward you to a very long Google URL with search    
results on how to iron a shirt.
### Mandatory Requirements
· Design and implement an API for short URL creation    
· Implement forwarding of short URLs to the original ones    
· There should be some form of persistent storage    
· The application should be distributed as one or more Docker images
#### Additional Requirements
· Design and implement an API for gathering different statistics
### Assessment
Treat this as a real project. It should be readable, maintainable, and extensible where    
appropriate. The implementation should preferably be in Java, however any language can be used.    
If you transfer it to another team - it should be clear how to work with it and    
what is going on. You should send us a link to a Git repository that we will be able to clone.

### Built With
* [Java](https://www.java.com/en/)
* [Spring Boot](https://spring.io/projects/spring-boot)
* [Maven](https://maven.apache.org/)
* [Thymeleaf](https://www.thymeleaf.org/)
* [Docker](https://www.docker.com/)
* [Docker-Compose](https://docs.docker.com/compose/)
* [MySQL](https://www.mysql.com/)

### Application Endpoints
| Endpoint|Mapping|purpose|
|----------|:------:|-------------:|
|/createaurl|POST|Add a new entry.|
|/updateaurl|POST|Update a URL that exists currently|
|/delete|POST|Delete an existing entry|
|/{shortUrl}|GET|Redirect to an external address - where the short url exists - redirects to the full address|


### Statistic Endpoints
| Endpoint|Purpose|
|----------|:-------------:|
|/actuator/health|Shows application health information.|
|/actuator/info|Displays arbitrary application info.|
|/actuator/metrics|Shows ‘metrics’ information for the current application.|
|/actuator/mappings|Displays a collated list of all @RequestMapping paths.|
|/actuator/env|Exposes properties from Spring’s ConfigurableEnvironment.|
|/actuator/conditions|Shows the conditions that were evaluated on configuration and auto-configuration classes and the reasons why they did or did not match.|


<!-- GETTING STARTED -->    
## Getting Started

### Prerequisites

* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [Docker](https://docs.docker.com/docker-for-windows/install/)  - Windows Installation
* [Docker](https://runnable.com/docker/install-docker-on-linux) - Linux Installation
* [Docker-Compose](https://docs.docker.com/compose/install/)
* [Docker-machine](https://docs.docker.com/machine/install-machine/) - Only Required when testing

### Installation
Clone the repo:
```bash 
  sudo git clone https://gitlab.com/nlteshade/neueda-assignment.git && cd neueda-assignment
``` 
Build the docker containers:
```bash 
  sudo docker-compose up --build
``` 
Go to the following link to view the application
http://localhost:8080/


<!-- CONTACT -->    
## Contact

Michael Kidd -  kidd_michael@outlook.com

Project Link: [https://gitlab.com/nlteshade/neueda-assignment/](https://gitlab.com/nlteshade/neueda-assignment/)
